import modalComponent from './modal.vue';
import lists from './lists.vue';
import message from './message.vue';
import dmMessage from './dmMessage.vue';
import dmList from './dmList.vue';
import users from './users.vue';
import dropList from './dropList.vue';

export default{
  modalComponent: modalComponent,
  lists: lists,
  message: message,
  dmMessage: dmMessage,
  dmList: dmList,
  users: users,
  dropList: dropList
};
