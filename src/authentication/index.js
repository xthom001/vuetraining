import config from "../config.js";
// Going to need to read up on vuex
// GOing to need to find a way to share state throughout the app
// Going to need it because my components need access to the state
export default{
  authorizedUser: {
    "name": '',
    "password": '',
    "userName": '',
    "userType": ''
  },
  isAuthorized(){
    return !(Object.keys(this.authorizedUser).length === 0 && JSON.stringify(this.authorizedUser) === JSON.stringify({}));
  },
  authenticate(username,password,vueInstance){
    var self = this;
    config.firebase.child('users').orderByKey().on("value", function(data){
      var users = data.val();
      for(var prop in users){
        var userObj = users[prop];
        if(username === userObj.userName && password === userObj.password){
          if(userObj.userType !== "normal"){
            self.authorizedUser = userObj;
            vueInstance.$router.go({
              path: '/adminView/' + userObj.userType,
            });
          }else{
            self.authorizedUser = userObj;
            vueInstance.$router.go({
              path: '/user/' + userObj.name,
            });
          }
        }
      }
      vueInstance.resetForm();
    });
  },
  signup(username,password,name,vueInstance){
    config.firebase.child('users').push({
      name: username.name,
      password: password.password,
      userName: name.userName,
      userType: "normal"
    });
    vueInstance.resetForm();
  }
};
