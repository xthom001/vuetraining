import Firebase from 'firebase';

export default {
  firebase: new Firebase("https://vuelearning.firebaseio.com/")
};
