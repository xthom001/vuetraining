import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import helpers from './helpers';

Vue.use(VueRouter);
helpers.filters(Vue);

var router = routes(VueRouter);

// Root component
var root = Vue.extend({});

// Boots straps the root of the application
router.start(root,'body', function(){
  router.go('/home');
});
