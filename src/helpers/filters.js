export default function(Vue){
    Vue.filter('date', function(value){
      var tempDate = new Date(value);
      return tempDate.toDateString();
    });
}
