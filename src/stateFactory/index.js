import server from './serverRequests';

export default {
  currentUser: server.currentUser,
  getAllPosts(){
    return server.getAllPosts();
  },
  deletePost(key){
    return server.deletePost(key);
  },
  getUsersPosts(temp){
    return server.getUsersPosts(temp);
  },
  getAnnouncments(){
    return server.getAnnouncments();
  },
  submitPost(message){
    server.submitPost(message);
  },
  getDmsList(){
    return server.getDmsList();
  },
  getDmMessage(key){
    return server.getDmMessage(key);
  },
  submitDms(key,message){
    server.submitDms(key,message);
  },
  getUserDetail(name){
    return server.getUserDetail(name);
  },
  getSubscriptions(userType){
    return server.getSubscriptions(userType);
  },
  createNewDm(dmContainer,message){
    return server.createNewDm(dmContainer,message);
  },
  deleteDm(key){
    server.deleteDm(key);
  }
};
