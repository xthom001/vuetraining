import config from "../config.js";
import auth from "../authentication";

export default{
  currentUser: {
    name: auth.authorizedUser.name,
    userType: auth.authorizedUser.userType
  },
  getAllPosts(){
    return config.firebase.child('posts').orderByKey();
  },
  getUsersPosts(user){
    var person = user || this.currentUser.name;
    return config.firebase.child('posts').orderByChild('owner').equalTo(person);
  },
  deletePost(key){
    return config.firebase.child('posts').child(key).remove();
  },
  getAnnouncments(){
    return config.firebase.child('posts').orderByChild('type').equalTo('announcement');
  },
  submitPost(message){
    config.firebase.child('posts').push(message);
  },
  getDmsList(){
    return config.firebase.child('dms').orderByKey();
  },
  getDmMessage(key){
    return config.firebase.child('dms').child(key);
  },
  submitDms(key,message){
    config.firebase.child('dms').child(key).child('messages').push(message);
  },
  getUserDetail(name){
    return config.firebase.child('users').orderByChild('name').equalTo(name);
  },
  getSubscriptions(userType){
    return config.firebase.child('users').orderByChild('userType').equalTo(userType);
  },
  createNewDm(dmContainer,message){
    var key = config.firebase.child('dms').push(dmContainer).key();
    config.firebase.child('dms').child(key).child('messages').push(message);
    return key;
  },
  deleteDm(key){
    config.firebase.child('dms').child(key).remove();
  }
}
