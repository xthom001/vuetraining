// import pages from "./pages"; // none async
import auth from './authentication';
export default function(VueRouter){
  var router = new VueRouter();
  var adminSections = "./pages/subSections/admin/";
  var commonSections = "./pages/common/";
  var userSections = "./pages/subSections/users/";

  router.map({
    '/home':{
      component: function(resolve){
        require(['./pages/landing.vue'], resolve);
      },
      auth: false
    },
    '/adminView/:userType':{
      component: function(resolve){
        require(['./pages/admin.vue'], resolve);
      },
      subRoutes:{
        '/home':{
          component: function(resolve){
            require([adminSections + 'home.vue'], resolve);
          },
          auth: true
        },
        '/usersLists':{
          component: function(resolve){
            require([adminSections + 'usersLists.vue'], resolve);
          },
          auth: true
        },
        '/usersFeeds':{
          component: function(resolve){
            require([commonSections + 'usersFeeds.vue'], resolve);
          },
          auth: true
        },
        '/announcements':{
          component: function(resolve){
            require([adminSections + 'announcements.vue'], resolve);
          },
          auth: true
        },
        '/dms':{
          component: function(resolve){
            require([commonSections + 'dms.vue'], resolve);
          },
          auth: true
        },
        '/adminPosts':{
          component: function(resolve){
            require([commonSections + 'posts.vue'], resolve);
          },
          auth: true
        }
      },
      auth: true
    },
    '/user/:user':{
      component: function(resolve){
        require([ './pages/users.vue'], resolve);
      },
      subRoutes:{
        '/home':{
          component: function(resolve){
            require([commonSections + 'usersFeeds.vue'], resolve);
          },
          auth: true
        },
        '/friendlist':{
          component: function(resolve){
            require([userSections + 'usersLists.vue'], resolve);
          },
          auth: true
        },
        '/dms':{
          component: function(resolve){
            require([commonSections + 'dms.vue'], resolve);
          },
          auth: true
        },
        '/posts':{
          component: function(resolve){
            require([commonSections + 'posts.vue'], resolve);
          },
          auth: true
        }
      },
      auth: true
    }
  });

  router.beforeEach(function(transition){
    if(transition.to.auth){
      if(auth.isAuthorized()){
        transition.next();
      }
    }else{
      transition.next();
    }
  });

  return router;
}
